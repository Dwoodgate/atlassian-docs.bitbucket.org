<!DOCTYPE html>
<html>
    <head>
        <title>Clover 4.1 : About Code Coverage</title>
        <link rel="stylesheet" href="styles/site.css" type="text/css" />
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <body class="theme-default aui-theme-default">
        <div id="page">
            <div id="main" class="aui-page-panel">
                <div id="main-header">
                    <div id="breadcrumb-section">
                        <ol id="breadcrumbs">
                            <li class="first">
                                <span><a href="index.html">Clover 4.1</a></span>
                            </li>
                                                    <li>
                                <span><a href="Clover-Documentation-Home_71598318.html">Clover Documentation Home</a></span>
                            </li>
                                                    <li>
                                <span><a href="About-Clover_71598399.html">About Clover</a></span>
                            </li>
                                                </ol>
                    </div>
                    <h1 id="title-heading" class="pagetitle">
                                                <span id="title-text">
                            Clover 4.1 : About Code Coverage
                        </span>
                    </h1>
                </div>

                <div id="content" class="view">
                    <div class="page-metadata">
                        
        
    
        
    
        
        
            Created by <span class='author'> Rosie Jameson [Atlassian]</span>, last modified by <span class='editor'> Marek Parfianowicz</span> on Apr 11, 2014
                        </div>
                    <div id="main-content" class="wiki-content group">
                    <h3 id="AboutCodeCoverage-WhatisCodeCoverage?">What is Code Coverage?</h3><p><em>Code coverage</em> is the percentage of code which is covered by automated tests. <em>Code coverage measurement</em> simply determines which statements in a body of code have been executed through a test run, and which statements have not. In general, a code coverage system collects information about the running program and then combines that with source information to generate a report on the test suite's code coverage.</p><p>Code coverage is part of a feedback loop in the development process. As tests are developed, code coverage highlights aspects of the code which may not be adequately tested and which require additional testing. This loop will continue until coverage meets some specified target.</p><h3 id="AboutCodeCoverage-WhyMeasureCodeCoverage?">Why Measure Code Coverage?</h3><p>It is well understood that unit testing improves the quality and predictability of your software releases. Do you know, however, how well your unit tests actually test your code? How many tests are enough? Do you need more tests? These are the questions code coverage measurement seeks to answer.</p><p>Coverage measurement also helps to avoid test entropy. As your code goes through multiple release cycles, there can be a tendency for unit tests to atrophy. As new code is added, it may not meet the same testing standards you put in place when the project was first released. Measuring code coverage can keep your testing up to the standards you require. You can be confident that when you go into production there will be minimal problems because you know the code not only passes its tests but that it is well tested.</p><p>In summary, we measure code coverage for the following reasons:</p><ul><li>To know how well our tests actually test our code</li><li>To know whether we have enough testing in place</li><li>To maintain the test quality over the lifecycle of a project</li></ul><p>Code coverage is not a panacea. Coverage generally follows an 80-20 rule. Increasing coverage values becomes difficult, with new tests delivering less and less incrementally. If you follow defensive programming principles, where failure conditions are often checked at many levels in your software, some code can be very difficult to reach with practical levels of testing. Coverage measurement is not a replacement for good code review and good programming practices.</p><p>In general you should adopt a sensible coverage target and aim for even coverage across all of the modules that make up your code. Relying on a single overall coverage figure can hide large gaps in coverage.</p><h3 id="AboutCodeCoverage-HowCodeCoverageWorks">How Code Coverage Works</h3><p>There are many approaches to code coverage measurement. Broadly there are three approaches, which may be used in combination:</p><div class="table-wrap"><table class="confluenceTable"><tbody><tr><td class="confluenceTd"><p><strong>Source code instrumentation</strong></p></td><td class="confluenceTd"><p>This approach adds instrumentation statements to the source code and compiles the code with the normal compile tool chain to produce an instrumented assembly.</p></td></tr><tr><td class="confluenceTd"><p><strong>Intermediate code instrumentation</strong></p></td><td class="confluenceTd"><p>Here the compiled class files are instrumented by adding new bytecodes, and a new instrumented class is generated.</p></td></tr><tr><td class="confluenceTd"><p><strong>Runtime information collection</strong></p></td><td class="confluenceTd"><p>This approach collects information from the runtime environment as the code executes to determine coverage information</p></td></tr></tbody></table></div><p>Clover uses source code instrumentation, because although it requires developers to perform an instrumented build, source code instrumentation produces the most accurate coverage measurement for the least runtime performance overhead.</p><p>Be aware that while Clover is capable of instrumenting both Java and Groovy source code, the instrumentation stage occurs prior to compilation with Java and during compilation with Groovy.</p><p>As the code under test executes, code coverage systems collect information about which statements have been executed. This information is then used as the basis of reports. In addition to these basic mechanisms, coverage approaches vary on what forms of coverage information they collect. There are many forms of coverage beyond basic statement coverage including conditional coverage, method entry and path coverage.</p><h3 id="AboutCodeCoverage-CodeCoveragewithClover">Code Coverage with Clover</h3><p>Clover is designed to measure code coverage in a way that fits seamlessly with your current development environment and practices, whatever they may be. Clover's IDE Plugins provide developers with a way to quickly measure code coverage without having to leave the IDE. Clover's Ant and Maven integrations allow coverage measurement to be performed in Automated Build and Continuous Integration systems, and reports generated to be shared by the team.</p><h4 id="AboutCodeCoverage-TypesofCoveragemeasured">Types of Coverage measured</h4><p>Clover measures three basic types of coverage analysis:</p><div class="table-wrap"><table class="confluenceTable"><tbody><tr><td class="confluenceTd"><p><strong>Statement</strong></p></td><td class="confluenceTd"><p>Statement coverage measures whether each statement is executed.</p></td></tr><tr><td class="confluenceTd"><p><strong>Branch</strong></p></td><td class="confluenceTd"><p>Branch coverage (sometimes called Decision Coverage) measures which possible branches in flow control structures are followed. Clover does this by recording if the boolean expression in the control structure evaluated to both true and false during execution.<br class="atl-forced-newline"/> <br class="atl-forced-newline"/> In Groovy code, Clover also treats Elvis expressions (a :? b), safe method calls (a?.b()), safe property calls (a?.b) and safe attribute calls (a?.@b) as branches.</p></td></tr><tr><td class="confluenceTd"><p><strong>Method</strong></p></td><td class="confluenceTd"><p>Method coverage measures if a method was entered at all during execution.</p></td></tr></tbody></table></div><p>Clover uses these measurements to produce a Total Coverage Percentage for each class, file, package and for the project as a whole. The Total Coverage Percentage allows entities to be ranked in reports. The Total Coverage Percentage (TPC) is calculated as follows:</p><div class="code panel pdl" style="border-width: 1px;"><div class="codeContent panelContent pdl">
<pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">TPC = (BT + BF + SC + MC)/(2*B + S + M) * 100%

where

BT - branches that evaluated to &quot;true&quot; at least once
BF - branches that evaluated to &quot;false&quot; at least once
SC - statements covered
MC - methods entered

B - total number of branches
S - total number of statements
M - total number of methods
</pre>
</div></div><h4 id="AboutCodeCoverage-WhataboutLineCoveragemetric?">What about Line Coverage metric?</h4><p>A <em>Line Coverage</em> metric is a basic metric offered by bytecode instrumentation tools, such as Cobertura or Emma, and it's tightly related with a fact that inside compiled classes we can have only information about line numbers (instead of information about code elements, such as statements etc).</p><p>As Clover uses source code instrumentation, it actually &quot;sees&quot; a real code structure. Therefore, Clover offers a <em>Statement Coverage</em> metric, which is similar to a Line Coverage metric in terms of it's granularity and precision.</p><h3 id="AboutCodeCoverage-Furtherreading">Further reading</h3><ul><li><a href="79986998.html">Why does Clover use source code instrumentation?</a></li><li><a href="About-Clover-code-metrics_306350640.html">About Clover code metrics</a></li></ul><p> </p>
                    </div>

                    
                 
                </div>             </div> 
            <div id="footer" role="contentinfo">
                <section class="footer-body">
                    <p>Document generated by Confluence on Apr 10, 2017 08:41</p>
                    <div id="footer-logo"><a href="http://www.atlassian.com/">Atlassian</a></div>
                </section>
            </div>
        </div>     </body>
</html>
