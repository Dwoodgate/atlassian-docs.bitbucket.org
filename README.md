# Atlassian documentation #

The documentation in this repository is for products that are no longer supported by Atlassian. This documentation is not maintained by Atlassian, but is open source and can be updated by anyone who wants to help improve it. 

## License

Documentation is licensed under [Creative Commons 2.5 Australia License](https://creativecommons.org/licenses/by/2.5/au/).

(c) Copyright 2015-2017 Atlassian.

## Contributing

Feel free to fork this repository and contribute via pull requests. In case this is your first contribution, 
please sign Atlassian Contributor License Agreement:

* [Corporate CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [Individual CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


# Read the documentation

[Atlassian documentation](http://atlassian-docs.bitbucket.io/)
